var db = require('../db')
var login = db.model('Login',{
	fb_token: { type: String,unique:true},
	fb_id: { type: String,unique:true},
	date: { type: Date,required:true,default:Date.now}
})

module.exports = login
