var db = require('../db')
var user = db.model('User',{
	name: { type: String},
	fb_id: { type: String,unique:true},
	read_quote_no: { type: Array,"default" : []},
	date: { type: Date,required:true,default:Date.now}
})

module.exports = user
