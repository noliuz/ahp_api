var db = require('../db')
var author = db.model('Author',{
	name: { type: String,unique:true},
	bio: { type: String},
	img:{ data: Buffer,contentType:String},
	date: { type: Date,required:true,default:Date.now}
})
module.exports = author