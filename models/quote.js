var db = require('../db')
var Schema = db.Schema
var ObjectId = Schema.ObjectId

var quote = db.model('Quote',{
	no : {type:Number,unique:true,required:true},
	content: { type: String,unique:true},
	author_id: ObjectId,
	date: { type: Date,required:true,default:Date.now}
})
module.exports = quote