var async = require('async')
var Quote = require('../models/quote')
var Author = require('../models/author')

function put_add_quote(content,authorname,res) {
	var last_no = 0;
	var doc

	async.series([
		function(callback) {//get last quotes no
			
			Quote.findOne()
				 .sort({'date':-1})				 
				 .exec(function(err,docs) {
				 	if (err) {return next(err)}
				 	if (docs) {
				 		last_no = docs.no
				 	}
				 	callback()
				 })
		},
		function(callback) {//get author id 
			
			Author.findOne({'name':authorname})
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (docs) {
					quote2 = new Quote({
						content:content,
						author_id:docs._id,
						no:(last_no+1)
					})
					quote2.save()	
					return res.sendStatus(202)
				} else {
					console.log("dont know author name:"+authorname)
					return res.sendStatus(402)
				}

			})
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)
}

function addBulkQuote(content,authorname,last_no) {
	var doc

	async.series([		
		function(callback) {//get author_id
			Author.findOne({'name':authorname})
			.exec(function(err,docs) {
				if (err) {return console.log(err)}
				if (docs) {//save quote
					last_no += 1
					quote2 = new Quote({
						content:content,
						author_id:docs._id,
						no:(last_no)
					})
					quote2.save()	
					
				} else {
					console.log("dont know author name:"+authorname)
				}
				callback()
			})
		}],
		function(err) {
			if (err) return console.log(err)
			return 201
		}
	)
}

module.exports = {
	putAddQuote:put_add_quote,
	addBulkQuote:addBulkQuote
}