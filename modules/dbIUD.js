var User = require('../models/user')
var Login = require('../models/login')

var add_user = function(name,fb_id,read_quote_id_arr) {	

	user = new User({
		name:name,
		fb_id:fb_id,
		read_quote_id:read_quote_id_arr
	})
	user.save()	

	return user._id

}

var add_read_quote_no = function(user_id,author_id,read_quote_no_arr,quote_no) {

	User.findOne({'_id':user_id})
	.exec(function(err,docs) {// chk user_id
		if (err) {console.log(err)}
		if (!docs) {
			console.log('no user_id:'+user_id)
		} else {//add quote_id					
			
			//find author_id
			qNo = 0;
			for (i=0;i<read_quote_no_arr.length;i++) {
				if (read_quote_no_arr[i] == author_id.toString()) {
					read_quote_no_arr[i+1] = quote_no
					qNo = author_id
				}
			}
				//not found author_id
			if (qNo == 0) {
				no_arr = new Array()
				no_arr.push(author_id)
				no_arr.push(quote_no)
				
				docs.read_quote_no = no_arr
			} else {
				docs.read_quote_no = read_quote_no_arr	
			}

			//debug
			console.log('user docs: '+docs)
			
			docs.save()			
		}

	})
}

module.exports = {
	addUser:add_user,
	addReadQuoteNo:add_read_quote_no
}