var router = require('express').Router()
var async = require('async')
var debug = require('debug')('router')

var Author = require('../models/author')
var User = require('../models/user')
var Login = require('../models/login')
var Quote = require('../models/quote')

var gen = require('../modules/general')
var iud = require('../modules/dbIUD')

router.post('/api/get_next_quote',function(req,res,next) {
	var fb_id = req.body.fb_id
	var authorname = req.body.authorname
	var fb_id,read_quote_no_arr = {}
	var author_id,user_id	

	async.series([	
		function(callback) {//get read quotes array
			User.findOne({'fb_id':fb_id})
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (!docs) {// no user found
					user_id = iud.addUser("dummy",fb_id,[])					
					console.log("new user created:"+fb_id)	
					read_quote_no_arr = []			
				} else {//retrieve next quote							
					read_quote_no_arr = docs.read_quote_no
					user_id = docs._id
					console.log('read_quote_no_arr:'+read_quote_no_arr)
				}
				callback()
			})
		},
		function(callback) {//find author_id			
			if (!authorname) {
				author_id = ""
				console.log("author_id:"+author_id)
				callback()
			} else {
				console.log(authorname)
				Author.findOne({'name':authorname})
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (!docs) {
						return callback(new Error('get_next_quote no author_name:'+authorname))					
					} else {
						author_id = docs._id
						console.log("author_id:"+author_id)
					}
					callback()
				})
			}			
		},
		function(callback) {//retrieve a quote			
			
			qNum = 0;
			for (i=0;i<read_quote_no_arr.length;i++) {
				if (read_quote_no_arr[i] == author_id.toString()) {
					qNum = read_quote_no_arr[i+1]
					break;
				}				
			}
			console.log("qNum:"+qNum)

			if (author_id) {
				Quote.findOne(
					{
						'no':{'$gt':qNum},
						'author_id': author_id
					}
				)
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (docs) {
						console.log("quote:"+docs)

						iud.addReadQuoteNo(user_id,author_id,
							read_quote_no_arr,docs.no)						

						return res.json(docs)
					} else {
						console.log("quote not found:")
						return res.sendStatus(403)
					}
					callback()
				})
			} else {
				Quote.findOne(
					{
						'no':{'$gt':read_quote_no_arr[0]}				
					}
				)
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (docs) {
						console.log("quote:"+docs)
						iud.addReadQuoteNo(user_id,docs.no)
						//console.log("user_id:"+user_id+" quote_id:"+docs._id)

						return res.json(docs)
					} else {
						console.log("not found:")
						return res.sendStatus(403)
					}
					callback()
				})
			}
			
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)

})

router.post('/api/get_prev_quote',function(req,res,next) {
	var fb_id = req.body.fb_id
	var authorname = req.body.authorname
	var fb_id,read_quote_no_arr
	var author_id,user_id
	
	console.log('fb_id:'+fb_id)

	async.series([/*
		function(callback) {//chk if token existed
			Login.findOne({'fb_token':fb_token}) //chk valid session
			.exec(function(err,docs) {
				if (err) { return next(err)}
				if (!docs) {// reply no this token logged
					return callback(new Error('no this token:'+fb_token))
				} else {//get next quote
					fb_id = docs.fb_id
					console.log("fb_id:"+fb_id)
				}
				callback()	
			})
		},*/
		function(callback) {//get read quotes array
			User.findOne({'fb_id':fb_id})
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (!docs) {// no user found
					iud.addUser("dummy",fb_id,[])
					console.log("new user created:"+fb_id)	
					read_quote_no_arr = []				
				} else {//retrieve next quote							
					read_quote_no_arr = docs.read_quote_no
					user_id = docs._id
					console.log('read_quote_no_arr:'+read_quote_no_arr)
				}
				callback()
			})
		},
		function(callback) {//find author_id			
			if (!authorname) {
				author_id = ""
				console.log("author_id:"+author_id)
				callback()
			} else {
				console.log(authorname)
				Author.findOne({'name':authorname})
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (!docs) {
						return callback(new Error('get_next_quote no author_name:'+authorname))					
					} else {
						author_id = docs._id
						console.log("author_id:"+author_id)
					}
					callback()
				})
			}			
		},
		function(callback) {//retrieve a quote
			console.log("b:"+author_id+" "+read_quote_no_arr)
			
			//find old bookmark
			qNum = 0;
			for (i=0;i<read_quote_no_arr.length;i++) {
				if (read_quote_no_arr[i] == author_id.toString()) {
					qNum = read_quote_no_arr[i+1]
					break;
				}				
			}
			console.log("qNum:"+qNum)

			if (author_id) {
				Quote.findOne(
					{
						'no':{'$lt':qNum},
						'author_id': author_id
					}
				)
				.sort({no:-1})
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (docs) {
						console.log("quote_xx:"+docs)					

						iud.addReadQuoteNo(user_id,author_id,
							read_quote_no_arr,docs.no)
						

						return res.json(docs)
					} else {
						console.log("quote not found:")
						return res.sendStatus(403)
					}
					callback()
				})
			} else {
				Quote.findOne(
					{
						'no':{'$lt':read_quote_no_arr[0]}				
					}
				)
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (docs) {
						console.log("quote:"+docs)
						iud.addReadQuoteNo(user_id,docs.no)
						//console.log("user_id:"+user_id+" quote_id:"+docs._id)

						return res.json(docs)
					} else {
						console.log("not found:")
						return res.sendStatus(403)
					}
					callback()
				})
			}
			
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)

})


router.post('/api/get_next_quote_no_remember',function(req,res,next) {
	var fb_token = req.body.fb_token
	var authorname = req.body.authorname
	var prev_date = req.body.prev_date

	var fb_id
	var author_id,user_id

	
	console.log('fb_token:'+fb_token)

	async.series([
		function(callback) {//chk if token existed
			Login.findOne({'fb_token':fb_token}) //chk valid session
			.exec(function(err,docs) {
				if (err) { return next(err)}
				if (!docs) {// reply no this token logged
					return callback(new Error('no this token:'+fb_token))
				} else {//get next quote
					fb_id = docs.fb_id
					console.log("fb_id:"+fb_id)
				}
				callback()	
			})
		},
		function(callback) {//find author_id			
			if (!authorname) {
				author_id = ""
				console.log("author_id:"+author_id)
				callback()
			} else {
				console.log(authorname)
				Author.findOne({'name':authorname})
				.exec(function(err,docs) {
					if (err) {return next(err)}
					if (!docs) {
						return callback(new Error('get_next_quote no author_name:'+authorname))					
					} else {
						author_id = docs._id
						console.log("author_id:"+author_id)
					}
					callback()
				})
			}			
		},
		function(callback) {//retrieve a quote
			console.log("b:"+author_id)
			if (author_id) {
				if (prev_date) {
					Quote.findOne(
						{						
							'author_id': author_id
						}
					)
					.where('date').gt(prev_date)
					.exec(function(err,docs) {
						if (err) {return next(err)}
						if (docs) {
							console.log("quote:"+docs)

							//iud.addReadQuoteId(user_id,docs._id)
							//console.log("user_id:"+user_id+" quote_id:"+docs._id)

							return res.json(docs)
						} else {
							console.log("quote not found:")
							return res.sendStatus(403)
						}
						callback()
					})
				} else {
					Quote.findOne(
						{						
							'author_id': author_id
						}
					)					
					.exec(function(err,docs) {
						if (err) {return next(err)}
						if (docs) {
							console.log("quote:"+docs)

							//iud.addReadQuoteId(user_id,docs._id)
							//console.log("user_id:"+user_id+" quote_id:"+docs._id)

							return res.json(docs)
						} else {
							console.log("quote not found:")
							return res.sendStatus(403)
						}
						callback()
					})
				}
			} else {
				if (prev_date) {
					Quote.findOne(
						
					).where('date').gt(prev_date)
					.exec(function(err,docs) {
						if (err) {return next(err)}
						if (docs) {
							console.log("quote:"+docs)
							//iud.addReadQuoteNo(user_id,docs._id)
							//console.log("user_id:"+user_id+" quote_id:"+docs._id)

							return res.json(docs)
						} else {
							console.log("not found:")
							return res.sendStatus(403)
						}
						callback()
					})
				} else {
					Quote.findOne(
						
					)
					.exec(function(err,docs) {
						if (err) {return next(err)}
						if (docs) {
							console.log("quote:"+docs)
							//iud.addReadQuoteId(user_id,docs._id)
							//console.log("user_id:"+user_id+" quote_id:"+docs._id)

							return res.json(docs)
						} else {
							console.log("not found:")
							return res.sendStatus(403)
						}
						callback()
					})
				}
			}
			
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)

})

router.post('/api/search_quote',function(req,res,next) {
	var fb_token = req.body.fb_token
	var keyword = req.body.keyword
	var fb_id
	var user_id

	
	console.log('fb_token:'+fb_token)

	async.series([
		function(callback) {//chk if token existed
			Login.findOne({'fb_token':fb_token}) //chk valid session
			.exec(function(err,docs) {
				if (err) { return next(err)}
				if (!docs) {// reply no this token logged
					return callback(new Error('no this token:'+fb_token))
				} else {//get next quote
					fb_id = docs.fb_id
					console.log("fb_id:"+fb_id)
				}
				callback()	
			})
		},
		function(callback) {//retrieve a quote
			Quote.find({
				'content': new RegExp(keyword,'i')
			})						
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (docs) {
					console.log("search quote:"+keyword+":"+docs)			
					return res.json(docs)
				} else {
					console.log("quote not found:")
					return res.sendStatus(403)
				}
				callback()
			})
				
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)

})

router.post('/api/search_author',function(req,res,next) {
	var fb_token = req.body.fb_token
	var keyword = req.body.keyword

	var fb_id
	var user_id

	
	console.log('fb_token:'+fb_token)

	async.series([
		function(callback) {//chk if token existed
			Login.findOne({'fb_token':fb_token}) //chk valid session
			.exec(function(err,docs) {
				if (err) { return next(err)}
				if (!docs) {// reply no this token logged
					return callback(new Error('no this token:'+fb_token))
				} else {//get next quote
					fb_id = docs.fb_id
					console.log("fb_id:"+fb_id)
				}
				callback()	
			})
		},
		function(callback) {//retrieve authors
			Author.find({
				$or:[
					{'name': new RegExp(keyword,'i')},
					{'bio' : new RegExp(keyword,'i')}
				]
			})						
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (docs) {
					console.log("search author:"+keyword+":"+docs)			
					return res.json(docs)
				} else {
					console.log("author not found:")
					return res.sendStatus(403)
				}
				callback()
			})
				
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)

})

module.exports = router