var router = require('express').Router()
var async = require('async')
var Quote = require('../models/quote')
var Author = require('../models/author')
var put = require('../modules/put')

router.post('/api/add_quote',function(req,res) {
	
	var content = req.body.content
	var authorname = req.body.authorname

	var last_no = 0;
	var doc

	async.series([
		function(callback) {//get last quotes no
			
			Quote.findOne()
				 .sort({'date':-1})				 
				 .exec(function(err,docs) {
				 	if (err) {return next(err)}
				 	if (docs) {
				 		last_no = docs.no
				 	}
				 	callback()
				 })
		},
		function(callback) {//get author id 
			
			Author.findOne({'name':authorname})
			.exec(function(err,docs) {
				if (err) {return next(err)}
				if (docs) {
					quote2 = new Quote({
						content:content,
						author_id:docs._id,
						no:(last_no+1)
					})
					quote2.save()	
					return res.sendStatus(202)
				} else {
					console.log("dont know author name:"+authorname)
					return res.sendStatus(402)
				}

			})
		}],
		function(err) {
			if (err) return next(err)
			res.sendStatus(201)	
		}
	)
})



router.post('/api/delete_quote',function(req,res) {
	objId = req.body.objId

	Quote.findOne({'_id':objId})
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				docs.remove()
				res.sendStatus(202)			
			} else {
				console.log("dont know ObjId")
				return res.sendStatus(402)
			}

		})
})

router.post('/api/get_quote',function(req,res) {
	Quote.find()
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				console.log(docs)
				return res.json(docs)	
			} else {
				console.log("dont know ObjId")
				return res.sendStatus(402)
			}

		})
})

router.post('/api/update_quote',function(req,res,next) {
	var objId = req.body.objId
	var content = req.body.content
	var authorname = req.body.authorname
	var author_id

	async.series([
		//find author id
		function(callback) {
			
			Author.findOne({'name':authorname})
			.exec(function(err,docs) {
				if (err) {return callback(err)}
				if (!docs) {
					return callback(new Error('dont know author name:'
						+authorname))
				}
				author_id = docs._id
				callback()
			})		

		},
		function(callback) {
			updateQuoteByObjId(callback,objId,content,author_id)
		}
	], function(err) {
		if (err) return next(err)
		res.sendStatus(201)	
	})
	
})

module.exports = router

function updateQuoteByObjId(callback,objId,content,author_id) {
	Quote.findOne({'_id':objId})
	.exec(function(err,docs) {						
		if (err) {return next(err)}			
		if (!docs) {
			return callback(new Error('no quote objId:'+objId))
		}

		docs.content = content
		docs.author = author_id
		docs.date = Date.now()
		docs.save()
	
		callback()
	})
}