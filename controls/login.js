var router = require('express').Router()
var async = require('async')
var Author = require('../models/author')
var User = require('../models/user')
var Login = require('../models/login')
var gen = require('../modules/general')

router.post('/api/send_token',function(req,res) {
	var fb_token = req.body.fb_token
	var fb_id = req.body.fb_id

	Login.findOne({'fb_id':fb_id}) //chk fb_id existed?
		.exec(function(err,docs) {
			if (err) { return next(err)}
			if (!docs) {// add new user
				login = new Login({
					fb_id: fb_id,
					fb_token: fb_token

				})
				login.save()
				res.sendStatus(202)
			} else {//user login already
				console.log('user login already fb_id:'+fb_id)
				res.sendStatus(401)
			}
		})
	
})

router.post('/api/delete_token',function(req,res) {
	var fb_token = req.body.fb_token
	
	Login.findOne({'fb_token':fb_token}) //chk fb_id existed?
		.exec(function(err,docs) {
			if (err) { return next(err)}
			if (docs) {
				docs.remove()
				res.sendStatus(202)
			} else {//no this token
				console.log('no this fb token'+fb_token)
				res.sendStatus(401)
			}
		})
})

module.exports = router

