var router = require('express').Router()
var async = require('async')
var Author = require('../models/author')
var User = require('../models/user')

router.post('/api/add_user',function(req,res) {
	name = req.body.name
	fb_id = req.body.fb_id
	read_quote_no = req.body.read_quote_no
	read_quote_no_arr = string2Array(read_quote_no)

	user = new User({
		name:name,
		fb_id:fb_id,
		read_quote_no:read_quote_no_arr
	})
	user.save()	
	return res.sendStatus(202)
	
})

router.post('/api/delete_user',function(req,res) {
	objId = req.body.objId

	User.findOne({'_id':objId})
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				docs.remove()
				res.sendStatus(202)			
			} else {
				console.log("dont know user ObjId:"+objId)
				return res.sendStatus(402)
			}

		})
})

router.post('/api/update_user',function(req,res,next) {
	var objId = req.body.objId
	var name = req.body.name
	var fb_id = req.body.fb_id
	var read_quote_no_arr = string2Array(req.body.read_quote_no)

	User.findOne({'_id':objId})
	.exec(function(err,docs) {
		if (err) {return callback(err)}
		if (!docs) {
			console.log("no user objId:"+objId)
			return res.sendStatus(401)
		} else {
			docs.name = name
			docs.fb_id = fb_id
			docs.read_quote_no = read_quote_no_arr
			docs.save()
			return res.sendStatus(202)
		}
	})		
	
})

module.exports = router

function string2Array(str) {
	return str.split(",")
}