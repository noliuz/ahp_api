var router = require('express').Router()
var fs = require('fs')
var Author = require('../models/author')
var async = require('async')

router.post('/api/add_author',function(req,res) {
	
		name = req.body.name
		bio = req.body.bio
		picname = req.body.picname


		a2 = new Author({
						name:name,
						bio:bio
					})

		if (!picname) {
			console.log("dont forget picname");
			return res.sendStatus(405)
		} 

		a2.img.data = fs.readFileSync('./img/'+picname)
		a2.img.contentType = 'img/png'
		//debug
		/*
		fs.writeFile("./tmp.txt",a2.img.data,function(err) {
			if (err)
				console.log("write file err")
		})*/

		a2.save()
		
		return res.sendStatus(202)
})

router.post('/api/delete_author',function(req,res) {
	
		objId = req.body.objId

		Author.findOne({'_id':objId})
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				docs.remove()
				res.sendStatus(202)			
			} else {
				console.log("dont know Author ObjId:"+objId)
				return res.sendStatus(402)
			}

		})
		
})

router.post('/api/update_author',function(req,res) {
	
		var objId = req.body.objId
		var name = req.body.name
		var bio = req.body.bio
		async.series([
			function(callback) {
				updateAuthorByObjId(callback,objId,name,bio)
			}],
			function(err) {
				if (err) return next(err)
				res.sendStatus(201)	
			})
		
		
})

router.post('/api/get_author',function(req,res) {
		Author.find() 
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				console.log(docs)
				return res.json(docs)	
			} else {
				console.log("dont know ObjId")
				return res.sendStatus(402)
			}

		})
})

router.post('/api/get_author_images',function(req,res) {
		Author.find() 
		.exec(function(err,docs) {
			if (err) {return next(err)}
			if (docs) {
				var reply=[];
				docs.forEach(function(doc) {
					tmp = {
						'id':doc.id,
						'img':doc.img
					}
					reply.push(tmp)
				}) 

				
				console.log(reply)
				return res.json(reply)	
			} else {
				console.log("dont know ObjId")
				return res.sendStatus(402)
			}

		})
})

function updateAuthorByObjId(callback,objId,name,bio) {
	Author.findOne({'_id':objId})
	.exec(function(err,docs) {						
		if (err) {return next(err)}			
		if (!docs) {
			return callback(new Error('no author objId:'+objId))
		}

		docs.name = name
		docs.bio = bio
		docs.date = Date.now()
		docs.save()
	
		callback()
	})
}

module.exports = router