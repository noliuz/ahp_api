var fs = require("fs")
var put = require('./modules/put')
var async = require('async')
var Converter=require("csvtojson").core.Converter;
var Quote = require('./models/quote')


//File name of quotes (.csv with headers (no field do not use))
var csvFileName="quotes.csv";
var fileStream=fs.createReadStream(csvFileName);
var csvConverter=new Converter({constructResult:true});
 
//end_parsed will be emitted once parsing finished 
csvConverter.on("end_parsed",function(objs){
   
	var last_no
	
	async.series([
		function(callback) {//get last no
			Quote.findOne()				 
			 .sort({'date':-1})				 
			 .exec(function(err,docs) {				 	
			 	if (err) {return console.log(err)}
			 	if (docs) {
			 		last_no = docs.no
			 		console.log("last_no=" + last_no)
			 	} else {
			 		last_no = 0
			 	}
			 	callback()
			 })
		},
		function(callback) {//save to db			
			objs.forEach(function(i) {				
				put.addBulkQuote(i.content,i.authorname,++last_no)
				console.log(last_no)
			}) 		
			callback()
		}],function(err) {
			if (err) return console.log(err)			
		})
		
})



fileStream.pipe(csvConverter)