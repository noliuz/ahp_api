var express = require('express')
var bodyParser = require('body-parser')
var http = require('http')
var fs = require('fs')
var iud = require('./modules/dbIUD')

var app = express()
var port_no = 3000

//include('server_config.js')

app.use(bodyParser.json())


app.use(require('./controls/quote'))
app.use(require('./controls/author'))
app.use(require('./controls/user'))
app.use(require('./controls/login'))
app.use(require('./controls/process'))

//iud.addReadQuoteId('551d4905b20eaeeb454960b5','666')

server = app.listen(port_no,function() {
	console.log('server UP'+' '+port_no)
	//test
	
	////

	fs.writeFile(__dirname+'/start.log','started')
})

function include(file_) {
    with (global) {
        eval(fs.readFileSync(file_) + '');
    };
};